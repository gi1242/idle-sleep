# idle-sleep: Sleep computers based on net/CPU activity.

These scripts help suspend your computer when idle, based on user, CPU and net
activity.

## idle-sleep.py

This is the main script and it performs the following functions:

1. Trigger an action when the user is away, and when the user returns
   (Default: nothing)

2. Trigger an action when the system looks idle based on user-input, net IO
   and CPU usage. (Default: suspend the system)

3. Trigger actions when the charger is plugged / unplugged. (Default:
   nothing)

4. Warn the user if the battery is low. (Default: below 10%)

5. Trigger an action when the battery is alarming low. (Default: hibernate the
   system at 5%)

I wrote this because my systems default power management interface doesn't
consider CPU/net activity when suspending. You may also find it useful if your
desktop doesn't have an integrated power-management interface.

## away-back.py

Helper script that dims the screen and suspends a list of processes when the
user is away, and undoes everything when the user returns. For example,
running 

    idle-sleep --away-timeout 120 \
        --away-cmd away-back --hoggers firefox-esr --away
        --back-cmd away-back --hoggers firefox-esr --back

Your screen will dim after 2 minutes of user inactivity and any running
firefox instances will be suspended. Your brightness will be restored and
Firefox will be continued as soon as user activity is detected.

## xwait.py

Helper script that waits for activity (up to an optional specified timeout)
and exits as soon as activity is detected, or the timeout is reached.

## Detecting idle systems.

Idle user input is detected using `dbus` or `XScreenSaver` extensions with
code directly taken from [Gajim](https://gajim.org). If it fails, the idle
time is reported as 0 so the system isn't accidentally suspended.

For net IO/CPU activity, this script polls periodically (at interval
lengths specified by `--poll-interval`), and then declares the system idle
based on the two methods:

* **Burst method:** The CPU/net IO should be at idle levels (specified by
  `--idle-bps` and `--idle-cpu`) for consecutive polls totaling at least the
  required system idle time (specified by the `--sys-idle` and
  `--sys-idle-batt` options). This method works fine for long poll intervals
  (minutes).

* **Exponential moving average:** An exponential moving average of the net IO
  and CPU usage should be below idle levels. The average is calibrated so that
  an initially active system (`--active-cpu` and `--active-bps`) that receives
  half the idle activity (`--idle-bps`, `--idle-cpu`) for a specified duration
  (`--sys-idle` / `--sys-idle-batt`) is declared idle. This method works much
  better for short polling interval (e.g. 5/10 seconds), but is less precise.
  Depending on activity the system may take a little longer / shorter to
  declare the system idle.

You can use any combination of these methods by the options `--use-ema`
(default), `--dont-use-ema`, `--use-burst`, `--dont-use-burst` (default).
If you don't use either method, the suspend action will be triggered solely
based on user activity. If you use both methods, the suspend action will be
triggered only if both methods declare the system to be idle.

## Idle / Back commands

If `--away-timeout` is non-zero, then this script runs `--away-cmd` if when
the user is away, and runs `--back-cmd` (if specified) when the user returns.
This is useful for dimming the screen if there is no keyboard activity for
sometime, for instance.

As a helper utility, you may find the supplied `xwait.py` script useful. It
simply waits for user activity up to a timeout (or forever if no timeout is
given), and then returns.

You may also find the supplied `away-back.py` script useful. This suspends a
given list of processes and dims the screen when the user is away, and
continues them and restores the screen brightness when the user is back.

## Web browsers that constantly ~~hog~~ use CPU.

Some websites will keep your CPU usage high 😒. This may cause your system to
never be declared idle and hence never suspend. Here are a few options:

1. Just use a high CPU threshold `--idle-cpu=10`. (Without Firefox running my
   typical idle CPU is 1% or lower. With firefox it hovers at 5%. When I'm
   running a long job, it's typically between 70 and 100%. So `--idle-cpu=10`
   works well for me.

2. Run a second instance of `idle-sleep` with a higher `--idle-cpu` threshold
   that only suspends your browser when the user is away, and continues it
   when the user returns:

        idle-sleep --away-cmd='pkill -STOP firefox-esr' \
            --back-cmd='pkill -STOP firefox-esr' \
            --away-timeout=300 --idle-cpu=20 \
            --warn-level=0 --sleep-cmd='' --alarm-cmd='' --battery-cmd='' \
            --ac-cmd=''

3. Ignore CPU activity entirely (`--idle-cpu=100`).


## Commands to run actions

By default all commands (e.g. `--sleep-cmd`, `--alarm-cmd`) are split on
white space and then executed directly. For anything fancier, enable the
`--use-shell` option to pass them to the default shell.

The standard input of all commands is `/dev/null`. If you're using `sudo`, you
should use `sudo -S`. If not, and there's a configuration error, `sudo` will
ask for a password on the controlling tty, and most likely get suspended by
the shell.

## Usage and Options

See the full usage using the `-h` option.

## Configuration file

Instead of using configuration options, you can specify options from a file by
prefixing the file name with `@` and entering one option per line. For
example, run

    idle-sleep.py @$HOME/.idle-sleep

and in `~/.idle-sleep` put whatever options you want to set:

    --user-idle=900
    --user-idle-batt=180
    --battery-cmd=xset s off dpms 150 0 0
    --ac-cmd=xset s off dpms 300 0 0
    --away-timeout=120
    --away-cmd=away-back --away --hoggers firefox-esr
    --back-cmd=away-back --back --hoggers firefox-esr
    --use-shell
    --warn-level=40
    --warn-cmd=echo "{date}: Battery at {percent:.2f}%. ({timeleft} left)" >> ~/.aosd-msgs

## Installation

Put `idle-sleep.py` and `gajim-sleep.py` in the same directory. Create a
symlink to `idle-sleep.py` from somewhere in your path.

## Licence

The code taken from [gajim](https://gajim.org) is distributed under the GPL.
Everything else is public domain.

