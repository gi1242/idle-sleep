#! /usr/bin/python3
# Dim screen and suspend CPU hoggers when away

import argparse, time, subprocess

global args, parser

def add_arg( *fargs, **kwargs ):
    if set( ['help', 'default'] ).issubset( kwargs.keys() ):
        kwargs['help'] += f' (default {kwargs["default"]}).'
    parser.add_argument( *fargs, **kwargs )

def run_cmd( cmd ):
    # run cmd
    if args.debug:
        print( time.strftime('%F %T:'),
                f'Running {cmd}...' )

    if type(cmd) == str: cmd = cmd.split()
    return subprocess.call( cmd, shell=False )

# Parse arguments
parser = argparse.ArgumentParser( description='''\
    Dim screen (and suspend CPU hoggers) when away, and restore when back.''',
    fromfile_prefix_chars='@'
    )

add_arg( '--hoggers', type=str, nargs='+', default=[''],
        help='CPU Hoggers to suspend when away' )
add_arg( '-d', '--dim', action='store_true', default=True,
        help='Dim screen when away' )
add_arg( '-D', '--dont-dim', action='store_false', dest='dim',
        help='Dim screen when away' )
add_arg( '-a', '--away', action='store_false', dest='back',
        help='User is away, dim screen and stop hoggers' )
add_arg( '-b', '--back', action='store_true', default=False,
        help='User is back, undo everything.' )
add_arg( '--debug', action='store_true', default=True,
        help='Debug' )
add_arg( '--dim-factor', type=float, default=.5,
        help='Factor to reduce brightness by when dimming' )

args = parser.parse_args()

if args.back:
    for h in args.hoggers:
        if h: run_cmd( f'pkill -CONT {h}' )

    if args.dim:
        subprocess.getoutput( 'brightnessctl -r' )

else:
    for h in args.hoggers:
        if h: run_cmd( f'pkill -STOP {h}' )
    
    if args.dim:
        (status, cur) = subprocess.getstatusoutput(
            'brightnessctl -m get' )
        if status == 0:
            new = int( int(cur)*args.dim_factor )
            if new > 0: run_cmd( f'brightnessctl -q -s set {new}' )
