#! /usr/bin/python3

import gajim_idle as idle
import time, psutil, argparse, signal, sys, os, subprocess, notify2
import types, atexit
import xwait

global args, parser

def add_arg( *fargs, **kwargs ):
    if set( ['help', 'default'] ).issubset( kwargs.keys() ):
        kwargs['help'] += f' (default {kwargs["default"]}).'
    parser.add_argument( *fargs, **kwargs )

def user_back():
    global input_grabbed, user_is_away

    if input_grabbed:
        xwait.ungrab_input()
        input_grabbed = False

    if user_is_away:
        user_is_away = False
        # TODO: We could get interrupted here, in which case back_cmd won't be
        # run.
        run_cmd(args.back_cmd)

@atexit.register
def run_back_cmd_if_needed():
    global user_is_away

    if 'user_is_away' in globals() and user_is_away:
        run_cmd(args.back_cmd)

def exit_handler(sig=None, frame=None):
    run_back_cmd_if_needed()
    print('Exiting.')
    sys.exit(0)

def ungrab_and_run_cmd( cmd ):
    # Declare user to be back, and then run cmd
    user_back()
    return run_cmd(cmd)

def run_cmd( cmd ):
    # run cmd
    if not cmd: return False    # Nothing to do

    if args.verbose:
        print( time.strftime('%F %T:'),
                f'Running {cmd}...' )
    try:
        with open( os.devnull, 'r' ) as null:
            if args.use_shell:
                subprocess.call( cmd, stdin=null, shell=True )
            else:
                subprocess.call( cmd.split(), stdin=null, shell=False )
        return True
    except Exception as e:
        print(e)
        return False

# Parse arguments
parser = argparse.ArgumentParser( description='''\
    Suspend system when idle. If the CPU / network usage are at at idle levels
    for a specified time, and if if the user been inactive for a specified
    time, then run a command (e.g. systemctl suspend). It also rings alarms and
    warns the user based on the battery levels. (Instead of using command line
    options you can specify options from a file by prefixing the file name with
    @ and entering one option per line.)''',
    fromfile_prefix_chars='@'
    )

add_arg( '-p', '--poll-interval', type=int, default=10,
        help='Interval to poll for network/CPU activity' )
add_arg( '-u', '--user-idle', type=int, default=10*60,
        help='User idle time before sleeping' )
add_arg( '-U', '--user-idle-batt', type=int, default=3*60,
        help='User idle time when on battery before sleeping' )

add_arg( '-b', '--idle-bps', type=int, default=10e3,
        help='Threshold (bytes/sec) well above net IO levels when idle' )
add_arg( '-C', '--idle-cpu', type=float, default=10,
        help='Threshold well above CPU %% when idle' )
add_arg( '-s', '--sys-idle', type=int, default=5*60,
        help='Net/CPU idle time before sleeping' )
add_arg( '-S', '--sys-idle-batt', type=int, default=2*60,
        help='Net/CPU idle time when on battery before sleeping' )

add_arg( '--use-burst', action='store_true', default=False,
        help='Require no bursts of Net/CPU before declaring system idle.' )
add_arg( '--dont-use-burst', action='store_false', dest='use_burst' )
add_arg( '--use-ema', action='store_true', default=True,
        help='Use an exponential moving average of Net/CPU to determine when the system is idle.' )
add_arg( '--dont-use-ema', action='store_false', dest='use_ema' )

add_arg( '--active-bps', type=int, default=1e6,
        help='Typical network traffic when active in bytes per sec, to calibrate exponential moving averages' )
add_arg( '--active-cpu', type=float, default=80,
        help='Typical CPU%% when active, to calibrate exponential moving averages' )

add_arg( '-c', '--sleep-cmd', type=str,
        default='sudo -S systemctl suspend',
        help='Command used to suspend the system' )
add_arg( '--sleep-delay', type=int, default=120,
        help='Minimum time between repeated runs of sleep-cmd' )

add_arg( '--away-timeout', type=int, default=0,
        help='User inactivity time after which away-cmd is run' )
add_arg( '--away-timeout-batt', type=int, default=0,
        help='User inactivity time after which away-cmd is run, when on battery' )
add_arg( '--away-cmd', type=str, default='',
        help='Command to run when user is away.' )
add_arg( '--back-cmd', type=str, default='',
        help='If specified, monitor user activity and run when user returns after being away.' )


add_arg( '--warn-level', type=float, default=10,
        help='Battery level below which to warn user' ),
add_arg( '--warn-cmd', type=str, default='',
        help='Command to run to issue warnings. Empty uses system notifications. Can use {percent}, {timeleft} and {date} in command' ),
add_arg( '--warn-delay', type=int, default=5*60,
        help='Minimum time between warnings' ),
add_arg( '--alarm-level', type=float, default=5,
        help='Battery level at which to run alarm command' ),
add_arg( '--alarm-delay', type=int, default=5*60,
        help='Minimum time between alarms' ),
add_arg( '--alarm-cmd', type=str,
        default='sudo -S systemctl hibernate',
        help='Command run when alarm rings' )

add_arg( '--battery-cmd', type=str,
        default=None,
        help='Command to run when plugged in' )
add_arg( '--ac-cmd', type=str,
        default=None,
        help='Command to run when unplugged' )

add_arg( '--use-shell', action='store_true', default=False,
        help='Use shell to run commands' )
add_arg( '--verbose', action='store_true', default=False,
        help='Be verbose' )
add_arg( '--debug', action='store_true', default=False,
        help='Print debugging info' )

args = parser.parse_args()

# Set relations between options
if args.debug: args.verbose=True
if args.away_timeout > 0 and args.away_timeout_batt == 0:
    args.away_timeout_batt = args.away_timeout

# Initialize
can_notify = notify2.init('idle-sleep')
if can_notify:
    notification = notify2.Notification( 'Idle notification' )

# Trap keyboard interrupt
user_is_away = False
signal.signal(signal.SIGINT, exit_handler)
signal.signal(signal.SIGTERM, exit_handler)

# Initialize counters
netio = psutil.net_io_counters()
(sent_c, recv_c) = (netio.bytes_sent, netio.bytes_recv)

# No counter needed. Repeated calls return load since last call.
cpu_percent = psutil.cpu_percent()

sys_idle_start = time.monotonic()
last_warn_time = None
last_alarm_time = None

prev_on_battery = None   # Start unknown

# Calibrating EMA:
# * Let n = ema_interval / poll_interval.
# * New avg = α(old avg) + (1-α)*new activity
# * Choose α so that if you start with activity at active levels and then only
#   have at idle/2, then after n steps you get to idle.
# * solving gives α = (idle / (2active - idle) )^{1/n}

ac_profile = types.SimpleNamespace(
    user_idle_sec=args.user_idle,
    sys_idle_sec=args.sys_idle,
    away_timeout=args.away_timeout,
    get_α = lambda δt: (2*args.active_bps/args.idle_bps -1)**
        (-δt/args.sys_idle),
    get_γ = lambda δt: (2*args.active_cpu/args.idle_cpu -1)**
        (-δt/args.sys_idle),
)
batt_profile = types.SimpleNamespace(
    user_idle_sec=args.user_idle_batt,
    sys_idle_sec=args.sys_idle_batt,
    away_timeout=args.away_timeout_batt,
    get_α = lambda δt: (2*args.active_bps/args.idle_bps -1)**
        (-δt/args.sys_idle_batt),
    get_γ=  lambda δt: (2*args.active_cpu/args.idle_cpu -1)**
        (-δt/args.sys_idle_batt),
)

ema_bps = args.active_bps
ema_cpu = args.active_cpu
last_sleep_time = None
last_poll_time = time.monotonic()

if idle.Monitor.is_available():
    get_idle_sec = idle.Monitor.get_idle_sec
else:
    print( 'Warning: Unable to detect user activity. (No DISPLAY?)' )
    get_idle_sec = lambda: 0

input_grabbed = False

while True:
    # Sleep / wait for user
    delay = max( 0.001,
            args.poll_interval + last_poll_time - time.monotonic() )

    if user_is_away and not input_grabbed and xwait.grab_input():
        input_grabbed = True

    if input_grabbed:
        if xwait.wait_for_user(delay): user_back()
    else:
        time.sleep( delay )

    # Compute activity 
    elapsed_time = time.monotonic() - last_poll_time
    last_poll_time += elapsed_time

    user_idle_sec = get_idle_sec()
    cpu_percent = psutil.cpu_percent()

    netio = psutil.net_io_counters()
    sent = max( netio.bytes_sent - sent_c, 0 )
    recv = max( netio.bytes_recv - recv_c, 0 )
    (sent_c, recv_c) = (netio.bytes_sent, netio.bytes_recv)

    if args.debug:
        # Debugging info
        print( f'{elapsed_time:.2f}s:',
                f'net={sent+recv}, CPU={cpu_percent:.1f}%',
                f'uidle={user_idle_sec}s',
                f'sidle={time.monotonic() - sys_idle_start:.1f}s',
                f'anet={ema_bps:.0f}Bps',
                f'acpu={ema_cpu:.1f}%'
            )

    # Check if we're on battery
    batt = psutil.sensors_battery()
    if batt is None or batt.power_plugged:
        # on AC power
        on_battery = False if batt is not None else None
        profile = ac_profile

        should_warn = False
        ring_alarm = False
    else:
        # On battery
        on_battery = True
        profile = batt_profile

        should_warn = (
            batt.percent < args.warn_level
            and ( last_warn_time is None
                or time.monotonic() - last_warn_time > args.warn_delay )
            )
        ring_alarm = (
            batt.percent < args.alarm_level
            and ( last_alarm_time is None
                or time.monotonic() - last_alarm_time > args.alarm_delay )
            )

    # Compute idle time, and decide if we should sleep
    if sent + recv > args.idle_bps * elapsed_time \
            or cpu_percent > args.idle_cpu:
        # Have net/cpu burst above idle levels. Reset idle start time
        sys_idle_start = time.monotonic()

    # Compute exponential moving averages of Net/CPU
    (α,γ) = (profile.get_α(elapsed_time), profile.get_γ(elapsed_time) )
    ema_bps *= α
    ema_bps += (1-α) * (sent + recv) / elapsed_time

    ema_cpu *= γ
    ema_cpu += (1-γ) * cpu_percent

    sys_idle = True
    if args.use_ema:
        sys_idle &= (ema_bps < args.idle_bps) and (ema_cpu < args.idle_cpu)
    if args.use_burst:
        sys_idle &= \
            (time.monotonic() - sys_idle_start > profile.sys_idle_sec )

    should_sleep = (user_idle_sec > profile.user_idle_sec) and sys_idle
    if last_sleep_time is not None:
        should_sleep &= (time.monotonic() - last_sleep_time > args.sleep_delay )

    # Plugged in / unplugged
    if on_battery != prev_on_battery:
        # Just plugged in / unplugged
        if on_battery: run_cmd( args.battery_cmd )
        else: run_cmd( args.ac_cmd )

        prev_on_battery = on_battery

    # Warn on battery levels
    if should_warn:
        timeleft=f'{batt.secsleft // 3600 :02d}:' \
            f'{(batt.secsleft%3600) // 60 :02d}:' \
            f'{batt.secsleft % 60 :02d}'
        if args.warn_cmd and run_cmd( args.warn_cmd.format(
                percent=batt.percent,
                timeleft=timeleft,
                date=time.strftime('%F %T') ) ):
            last_warn_time = time.monotonic()
        elif can_notify:
            try:
                notification.update( 'Warning',
                    f'Battery at {batt.percent:.2f}% ({timeleft} left).' )
                notification.show()
                last_warn_time = time.monotonic()
            except: pass

    # Ring alarm based on battery levels, or sleep based on idle
    if ring_alarm:
        if can_notify:
            try:
                notification.update( 'Critical',
                    f'Battery at {batt.percent:.2f}%' )
                notification.show()
            except: pass

        if ungrab_and_run_cmd( args.alarm_cmd ):
            last_alarm_time = time.monotonic()

    elif should_sleep:
        if( ungrab_and_run_cmd( args.sleep_cmd ) ):
            # time.monotonic() mostly ignores system sleep, so checking
            # differences in this should protect against repeatedly suspending
            # the system.
            last_sleep_time = time.monotonic()

    elif user_idle_sec > profile.away_timeout > 0 and sys_idle:
        if not user_is_away:
            run_cmd( args.away_cmd )
            # If back_cmd is specified, then declare user to be away and
            # monitor for activity. back_cmd will be run when the user returns.
            # If back_cmd is not specified, no monitoring is required.
            if args.back_cmd:
                user_is_away = True
