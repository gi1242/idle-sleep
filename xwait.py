#!/usr/bin/python3
# Wait for user activity and return.

import os, sys, time, select
from ctypes import byref, cast
from ctypes import POINTER, c_int, c_uint16, c_uint32, c_char

#import pyxtrlock.xcb as xcb
#import pyxtrlock.X as X
import xcb

def die(*message_parts, exit_code=1):
    """Print an error message to stderr and exit"""
    print("pyxtrlock:", *message_parts, file=sys.stderr)
    sys.exit(exit_code)

def init():
    conn = xcb.connect(None, None)
    xfd = xcb.get_file_descriptor(conn)

    if not conn:
        die("Could not connect to X server")

    screen_num = c_int()

    setup = xcb.get_setup(conn)

    iter_ = xcb.setup_roots_iterator(setup)

    while screen_num.value:
        xcb.screen_next(byref(iter_))
        screen_num.value -= 1

    screen = iter_.data.contents

    # create window
    window = xcb.generate_id(conn)

    mask = c_uint32( xcb.EVENT_MASK_KEY_PRESS | xcb.EVENT_MASK_BUTTON_PRESS
        | xcb.EVENT_MASK_POINTER_MOTION | xcb.EVENT_MASK_BUTTON_MOTION )
    attribs = (c_uint32 * 2)(1, mask)
    ret = xcb.create_window(conn, xcb.COPY_FROM_PARENT, window, screen.root,
            0, 0, 1, 1, 0, xcb.WINDOW_CLASS_INPUT_ONLY,
            xcb.VisualID(xcb.COPY_FROM_PARENT),
            xcb.CW_OVERRIDE_REDIRECT | xcb.CW_EVENT_MASK,
            cast(byref(attribs), POINTER(c_uint32)))

    return (conn, window, xfd)

def grab_input():
    # map window
    xcb.map_window(conn, window)

    # Grab keyboard
    # Use the method from the original xtrlock code:
    #  "Sometimes the WM doesn't ungrab the keyboard quickly enough if
    #  launching xtrlock from a keystroke shortcut, meaning xtrlock fails
    #  to start We deal with this by waiting (up to 100 times) for 10,000
    #  microsecs and trying to grab each time. If we still fail
    #  (i.e. after 1s in total), then give up, and emit an error"

    for i in range(100):
        try:
            status = xcb.grab_keyboard_sync(conn, 0, window,
                    xcb.CURRENT_TIME, xcb.GRAB_MODE_ASYNC,
                    xcb.GRAB_MODE_ASYNC)

            if status == xcb.GrabSuccess:
                break
            else:
                time.sleep(0.01)
        except xcb.XCBError as e:
            time.sleep(0.01)
    else:
        return False

    # Grab pointer
    for i in range(100):
        try:
            mask =  c_uint16( xcb.EVENT_MASK_BUTTON_PRESS \
                    | xcb.EVENT_MASK_POINTER_MOTION \
                    | xcb.EVENT_MASK_BUTTON_MOTION)
            status = xcb.grab_pointer_sync(conn, False, window, mask,
                    xcb.GRAB_MODE_ASYNC, xcb.GRAB_MODE_ASYNC,
                    xcb.WINDOW_NONE, xcb.NONE, xcb.CURRENT_TIME)

            if status == xcb.GrabSuccess:
                break
            else:
                time.sleep(0.01)
        except xcb.XCBError as e:
            time.sleep(0.01)
    else:
        # Grabbed keyboard, but couldn't grab pointer.
        xcb.ungrab_keyboard( conn, xcb.CURRENT_TIME )
        return False

    xcb.flush(conn)
    return True

def wait_for_user(timeout=None):
    # Input needs to be grabbed before calling, and ungrabbed when done.
    # Return true if if user returned, and false if user is still away.
    if timeout is not None and timeout < 0: timeout=0
    with select.epoll() as p:
        p.register( xfd, select.EPOLLIN )
        return bool( p.poll(timeout) )

def ungrab_input():
    xcb.ungrab_keyboard( conn, xcb.CURRENT_TIME )
    xcb.ungrab_pointer( conn, xcb.CURRENT_TIME )
    xcb.unmap_window( conn, window ) # Probably unnecessary
    xcb.flush(conn)

## Initialize
(conn, window, xfd) = init()

if __name__ == '__main__':
    try: timeout = int( sys.argv[1] )
    except: timeout = None
    
    if len( sys.argv) > 1 and timeout is None:
        print( 'Usage: xwait [timeout]\n' )
        print( 'Waits for user activity, and then returns.'
                '(Optional timeout in seconds)' )
    elif grab_input():
        wait_for_user(timeout)
        ungrab_input()
    else:
        print( 'Failed to grab input' )

